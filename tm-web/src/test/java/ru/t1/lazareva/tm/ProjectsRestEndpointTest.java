package ru.t1.lazareva.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.client.ProjectsRestEndpointClient;
import ru.t1.lazareva.tm.marker.IntegrationCategory;
import ru.t1.lazareva.tm.model.ProjectDto;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectsRestEndpointTest {

    @NotNull
    final ProjectsRestEndpointClient client = ProjectsRestEndpointClient.client();

    @NotNull
    final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    final ProjectDto project2 = new ProjectDto("Test Project 2");

    @NotNull
    final ProjectDto project3 = new ProjectDto("Test Project 3");

    @NotNull
    final List<ProjectDto> projects = Arrays.asList(project1, project2, project3);

    @NotNull
    final ProjectDto project4 = new ProjectDto("Test Project 4");

    @NotNull
    final ProjectDto project5 = new ProjectDto("test Project 5");

    @NotNull
    final List<ProjectDto> projects2 = Arrays.asList(project4, project5);

    @Before
    public void init() {
        client.post(projects);
    }

    @After
    public void clear() {
        client.delete();
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(client.get());
        for (final ProjectDto project : projects) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPost() {
        Assert.assertEquals(3, client.get().size());
        client.post(projects2);
        Assert.assertNotNull(client.get());
        Assert.assertEquals(5, client.get().size());
        for (final ProjectDto project : projects2) {
            Assert.assertNotNull(
                    client.get().stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void testPut() {
        for (final ProjectDto project : client.get()) {
            Assert.assertNull(project.getDescription());
        }
        projects.get(0).setDescription("Description 1");
        projects.get(1).setDescription("Description 2");
        projects.get(2).setDescription("Description 3");
        client.put(projects);
        for (final ProjectDto project : client.get()) {
            Assert.assertNotNull(project.getDescription());
        }
    }

    @Test
    public void testDelete() {
        Assert.assertNotNull(client.get());
        client.delete();
        Assert.assertEquals(Collections.emptyList(), client.get());
    }

}
