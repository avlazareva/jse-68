package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.dto.soap.*;

@Endpoint
public class TaskSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.lazareva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindOneByIdResponse taskFindOneById(@RequestPayload final TaskFindOneByIdRequest request) {
        return new TaskFindOneByIdResponse(taskService.findOneById(request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse taskSave(@RequestPayload final TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = NAMESPACE)
    public TaskUpdateResponse taskUpdate(@RequestPayload final TaskUpdateRequest request) {
        taskService.save(request.getTask());
        return new TaskUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse taskDelete(@RequestPayload final TaskDeleteRequest request) {
        taskService.removeOneById(request.getId());
        return new TaskDeleteResponse();
    }

}
