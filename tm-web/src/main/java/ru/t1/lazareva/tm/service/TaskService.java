package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.exception.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.NameEmptyException;
import ru.t1.lazareva.tm.model.TaskDto;
import ru.t1.lazareva.tm.repository.ITaskRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Override
    @Transactional
    public void save(@Nullable final TaskDto task) {
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        repository.save(task);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<TaskDto> tasks) {
        if (tasks.isEmpty()) throw new IdEmptyException();
        for (@NotNull TaskDto taskDto : tasks)
            if (taskDto.getName() == null || taskDto.getName().isEmpty()) throw new NameEmptyException();
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<TaskDto> optional = repository.findById(id);
        return optional.orElseThrow(EntityNotFoundException::new);
    }

}