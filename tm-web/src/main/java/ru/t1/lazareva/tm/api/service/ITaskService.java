package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.TaskDto;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void save(final TaskDto task);

    void saveAll(final Collection<TaskDto> tasks);

    void removeAll();

    void removeOneById(final String id);

    List<TaskDto> findAll();

    TaskDto findOneById(final String id);

}