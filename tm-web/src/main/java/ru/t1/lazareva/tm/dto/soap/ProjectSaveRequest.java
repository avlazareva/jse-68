package ru.t1.lazareva.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.lazareva.tm.model.ProjectDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveRequest")
public class ProjectSaveRequest {

    @XmlElement(required = true)
    protected ProjectDto project;

}