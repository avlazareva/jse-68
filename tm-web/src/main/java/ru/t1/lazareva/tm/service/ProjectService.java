package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.IProjectService;
import ru.t1.lazareva.tm.exception.EntityNotFoundException;
import ru.t1.lazareva.tm.exception.IdEmptyException;
import ru.t1.lazareva.tm.exception.NameEmptyException;
import ru.t1.lazareva.tm.model.ProjectDto;
import ru.t1.lazareva.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    @Transactional
    public void save(@Nullable final ProjectDto project) {
        if (project == null) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        repository.save(project);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<ProjectDto> projects) {
        if (projects == null) throw new IdEmptyException();
        if (projects.isEmpty()) throw new IdEmptyException();
        for (@NotNull ProjectDto projectDTO : projects)
            if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new NameEmptyException();
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDto> optional = repository.findById(id);
        return optional.orElseThrow(EntityNotFoundException::new);

    }

}