package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.model.TaskDto;

import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService
public class TaskRestEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @GetMapping("/{id}")
    public TaskDto get(@NotNull @PathVariable("id") String id) {
        return taskService.findOneById(id);
    }

    @PostMapping
    public void post(@NotNull @RequestBody TaskDto task) {
        taskService.save(task);
    }

    @PutMapping
    public void put(@NotNull @RequestBody TaskDto task) {
        taskService.save(task);
    }

    @DeleteMapping("/{id}")
    public void delete(@NotNull @PathVariable("id") String id) {
        taskService.removeOneById(id);
    }

}