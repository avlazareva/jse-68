package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.IProjectService;
import ru.t1.lazareva.tm.model.ProjectDto;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping("/{id}")
    public ProjectDto get(@NotNull @PathVariable("id") String id) {
        return projectService.findOneById(id);
    }

    @PostMapping
    public void post(@NotNull @RequestBody ProjectDto project) {
        projectService.save(project);
    }

    @PutMapping
    public void put(@NotNull @RequestBody ProjectDto project) {
        projectService.save(project);
    }

    @DeleteMapping("/{id}")
    public void delete(@NotNull @PathVariable("id") String id) {
        projectService.removeOneById(id);
    }

}