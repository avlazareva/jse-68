package ru.t1.lazareva.tm.api.service;

import ru.t1.lazareva.tm.model.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void save(final ProjectDto project);

    void saveAll(final Collection<ProjectDto> projects);

    void removeAll();

    void removeOneById(final String id);

    List<ProjectDto> findAll();

    ProjectDto findOneById(final String id);

}