package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.dto.soap.*;

@Endpoint
public class TasksSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.lazareva.t1.ru/dto/soap";

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "tasksFindAllRequest", namespace = NAMESPACE)
    public TasksFindAllResponse tasksFindAll() {
        return new TasksFindAllResponse(taskService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksSaveRequest", namespace = NAMESPACE)
    public TasksSaveResponse tasksSave(@RequestPayload final TasksSaveRequest request) {
        taskService.saveAll(request.getTasks());
        return new TasksSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksUpdateRequest", namespace = NAMESPACE)
    public TasksUpdateResponse tasksUpdate(@RequestPayload final TasksUpdateRequest request) {
        taskService.saveAll(request.getTasks());
        return new TasksUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "tasksDeleteRequest", namespace = NAMESPACE)
    public TasksDeleteResponse tasksDelete() {
        taskService.removeAll();
        return new TasksDeleteResponse();
    }

}