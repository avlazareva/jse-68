package ru.t1.lazareva.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.model.ProjectDto;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDto, String> {
}
