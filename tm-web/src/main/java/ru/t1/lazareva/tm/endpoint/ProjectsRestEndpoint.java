package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.IProjectService;
import ru.t1.lazareva.tm.model.ProjectDto;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @GetMapping()
    public List<ProjectDto> get() {
        return projectService.findAll();
    }

    @PostMapping
    public void post(@NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(projects);
    }

    @PutMapping
    public void put(@NotNull @RequestBody List<ProjectDto> projects) {
        projectService.saveAll(projects);
    }

    @DeleteMapping()
    public void delete() {
        projectService.removeAll();
    }

}