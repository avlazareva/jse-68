package ru.t1.lazareva.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.model.ProjectDto;

public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/project";

    static ProjectRestEndpointClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/{id}")
    ProjectDto get(@PathVariable("id") String id);

    @PostMapping
    void post(@RequestBody ProjectDto project);

    @PutMapping
    void put(@RequestBody ProjectDto project);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") String id);

}
