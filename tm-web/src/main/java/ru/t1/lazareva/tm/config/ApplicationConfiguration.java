package ru.t1.lazareva.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.lazareva.tm.endpoint.ProjectSoapEndpoint;
import ru.t1.lazareva.tm.endpoint.ProjectsSoapEndpoint;
import ru.t1.lazareva.tm.endpoint.TaskSoapEndpoint;
import ru.t1.lazareva.tm.endpoint.TasksSoapEndpoint;
import ru.t1.lazareva.tm.service.PropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@EnableWs
@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.lazareva.tm")
@EnableJpaRepositories("ru.t1.lazareva.tm.repository")
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDBDriver());
        dataSource.setUrl(propertyService.getDBUrl() + propertyService.getDBSchema());
        dataSource.setUsername(propertyService.getDBUser());
        dataSource.setPassword(propertyService.getDBPassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.lazareva.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDBDialect());
        properties.put(Environment.FORMAT_SQL, "true");
        properties.put(Environment.SHOW_SQL, propertyService.getDBShowSQL());
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        if ("true".equals(propertyService.getDBL2Cache())) {
            properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBL2Cache());
            properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheRegion());
            properties.put(Environment.USE_QUERY_CACHE, propertyService.getDBQueryCache());
            properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBMinimalPuts());
            properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
            properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheProvider());
        }
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "ProjectsEndpoint")
    public DefaultWsdl11Definition projectsWsdl11Definition(@NotNull final XsdSchema projectsEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectsSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectsSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectsSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(projectsEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TasksEndpoint")
    public DefaultWsdl11Definition tasksWsdl11Definition(@NotNull final XsdSchema tasksEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TasksSoapEndpoint.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TasksSoapEndpoint.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TasksSoapEndpoint.NAMESPACE);
        wsdl11Definition.setSchema(tasksEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    public XsdSchema projectsEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectsEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean
    public XsdSchema tasksEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/tasksEndpoint.xsd"));
    }

}
