package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.api.service.ITaskService;
import ru.t1.lazareva.tm.model.TaskDto;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @GetMapping()
    public List<TaskDto> get() {
        return taskService.findAll();
    }

    @PostMapping
    public void post(@NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(tasks);
    }

    @PutMapping
    public void put(@NotNull @RequestBody List<TaskDto> tasks) {
        taskService.saveAll(tasks);
    }

    @DeleteMapping()
    public void delete() {
        taskService.removeAll();
    }

}