package ru.t1.lazareva.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.model.ProjectDto;

import java.util.List;

public interface ProjectsRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects/";

    static ProjectsRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectsRestEndpointClient.class, BASE_URL);
    }

    @GetMapping()
    List<ProjectDto> get();

    @PostMapping
    void post(@RequestBody List<ProjectDto> projects);

    @PutMapping
    void put(@RequestBody List<ProjectDto> projects);

    @DeleteMapping()
    void delete();

}
