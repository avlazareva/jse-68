package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.TaskDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    @NotNull
    TaskDto create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    );

    @NotNull
    TaskDto create(@NotNull final String userId, @NotNull final String name);

    @Nullable
    List<TaskDto> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

}
