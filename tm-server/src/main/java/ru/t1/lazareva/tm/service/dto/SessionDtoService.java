package ru.t1.lazareva.tm.service.dto;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.dto.ISessionDtoService;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.repository.dto.SessionDtoRepository;

@Service
@NoArgsConstructor
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDto, SessionDtoRepository> implements ISessionDtoService {

}