package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDto;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    public TaskCreateResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
